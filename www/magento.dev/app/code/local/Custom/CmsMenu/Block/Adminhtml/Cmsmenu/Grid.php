<?php

class Custom_CmsMenu_Block_Adminhtml_Cmsmenu_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    
    protected function _prepareCollection() 
    {
        $collection = Mage::getModel('customcmsmenu/cmsmenu')->getCollection(); //Получаем модель коллекции menu
        print_r($collection);die;
        $this->setCollection($collection); //Устанавливаем коллекцию
        return parent::_prepareCollection();
    }
    
    protected function _prepareColumns() 
    {
        //Создаем колонки 
        $helper = Mage::helper('customcmsmenu');
        
        $this->addColumn('id', array(
            'header' => $helper->__('ID'),
            'index' => 'id'
        ));

        $this->addColumn('name', array(
            'header' => $helper->__('Name'),
            'index' => 'menu_name',
            'type' => 'text',
        ));

        $this->addColumn('data_created', array(
            'header' => $helper->__('Date Created'),
            'index' => 'created',
            'type' => 'date',
        ));
        
        $this->addColumn('data_update', array(
            'header' => $helper->__('Date Updated'),
            'index' => 'updated',
            'type' => 'date',
        ));
         
        $this->addColumn('status', array(
            'header'  => $helper->__('Status'),
            'index'   => 'status',
            'type'    => 'options',
            'options' => array(
                '0' => 'Disabled',
                '1' => 'Enabled'
            )
        ));

        return parent::_prepareColumns();
        
    }
    
    protected function _prepareMassaction()
    {
        //Возможность удаления нескольких меню
        $this->setMassactionIdField('id');//Устанавливаем id поля
        $this->getMassactionBlock()->setFormFieldName('menu'); //Устанавливаем название павраметра, которое будет использоваться при получении массива id полей

        $this->getMassactionBlock()->addItem('delete', array(
            'label' => $this->__('Delete'),
            'url' => $this->getUrl('*/*/massDelete'),
        )); //Происходит добавление опций в список массовых операций
        return $this;
    }
    
    public function getRowUrl($model)
    {
        //метод инициализации ссылки для каждой из строк Data Grid — реакция на клик пользователя по строке.
        return $this->getUrl('*/*/edit', array(
                    'id' => $model->getId(),
                ));
    }
    

}