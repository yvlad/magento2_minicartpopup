<?php

$installer = $this;
$tableCmsMenu = $installer->getTable('customcmsmenu/cmsmenu');
$tableCmsMenuLinks = $installer->getTable('customcmsmenu/cmslinks');

$installer->startSetup();

$installer->getConnection()->dropTable($tableCmsMenu);
$installer->getConnection()->dropTable($tableCmsMenuLinks);
$tableMenu = $installer->getConnection()
    ->newTable($tableCmsMenu)
    ->addColumn('id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'identity'  => true,
        'nullable'  => false,
        'primary'   => true,
        ))
    ->addColumn('menu_name', Varien_Db_Ddl_Table::TYPE_VARCHAR, '255', array(
        'nullable'  => false,
        ))
    ->addColumn('status', Varien_Db_Ddl_Table::TYPE_VARCHAR, null, array(
        'nullable'  => false,
        ))
    ->addColumn('created', Varien_Db_Ddl_Table::TYPE_DATETIME, null, array(
        'nullable'  => false,
    ))
    ->addColumn('updated', Varien_Db_Ddl_Table::TYPE_DATETIME, null, array(
        'nullable'  => true,
    ));

$tableLinks = $installer->getConnection()
    ->newTable($tableCmsMenuLinks)
    ->addColumn('links_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'identity'  => true,
        'nullable'  => false,
        'primary'   => true,
        ))
    ->addColumn('label', Varien_Db_Ddl_Table::TYPE_TEXT, '255', array(
        'nullable'  => false,
        ))
    ->addColumn('url_key', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
        'nullable'  => false,
        ))
    ->addColumn('position', Varien_Db_Ddl_Table::TYPE_VARCHAR, null, array(
        'nullable'  => false,
    ))
    ->addColumn('type', Varien_Db_Ddl_Table::TYPE_TINYINT, null, array(
        'nullable'  => false,
    ))
    ->addColumn('menu_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'nullable'  => false,
        ));
$installer->getConnection()->createTable($tableMenu);
$installer->getConnection()->createTable($tableLinks);

$installer->endSetup();


//    
//    ->addForeignKey(
//    $installer->getFkName(
//        'cminds_menus/link',
//        'menu_id',
//        'cminds_menus/menu',
//        'menu_id'
//    ),
//    'menu_id', $installer->getTable('cminds_menus/menu'), 'menu_id',
//    Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE);
//
//$installer->getConnection()->createTable($table);
//
//$installer->endSetup();