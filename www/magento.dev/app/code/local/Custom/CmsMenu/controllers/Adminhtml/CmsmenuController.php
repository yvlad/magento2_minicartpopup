<?php

class Custom_CmsMenu_Adminhtml_CmsmenuController extends Mage_Adminhtml_Controller_Action
{
    
    public function indexAction()
    {
        $this->loadLayout()->_setActiveMenu('customcmsmenu'); //Инициализируем макет страницы и Подсвечиваем вкладку
        $this->_addContent($this->getLayout()->createBlock('customcmsmenu/adminhtml_cmsmenu'));
            //Создаем блок который находится в модуле customcmsmenu по пути Block/Adminhtml/Cmsmenu.php и
            //Добавляем блок в макет в качестве содержимого  страницы
        $this->renderLayout();//Рендерит всю информацию   
    }
    
    public function newAction()
    {
        $this->_forward('edit');//Тут происходит редирект на метод edit
    }
    
    public function editAction()
    {
        $id = (int) $this->getRequest()->getParam('id');//Получаем id из GET
        
        $model = Mage::getModel('customcmsmenu/cmsmenu');

        if($data = Mage::getSingleton('adminhtml/session')->getFormData()){
            $model->setData($data)->setId($id); 
        }else{
            $model->load($id);
        }
        
        Mage::register('current_cmsmenu', $model); //Заносим в глобальный реестр current_menu   
        $this->loadLayout()->_setActiveMenu('customcmsmenu');//Инициализируем макет страницы и подсвечиваем вкладку
        $this->_addLeft($this->getLayout()->createBlock('customcmsmenu/adminhtml_cmsmenu_edit_tabs'));//Создаем блок табов,
        //Который находится  в модуле customcmsmenu по пути Block/Adminhtml/Cmsmenu/Edit/Tabs.php и
        //Добавляем блок табов в макет в качестве содержимого  страницы
        $this->_addContent($this->getLayout()->createBlock('customcmsmenu/adminhtml_cmsmenu_edit')); //Создаем блок,
        // который находится в модуле customcmsmenu по пути Block/Adminhtml/Cmsmenu/Edit.php и
            //Добавляем блок редактирования в макет в качестве содержимого  страницы
        $this->renderLayout();//Рендерит всю информацию 
    }
   
    public function saveAction()
    {
        if($data = $this->getRequest()->getPost()){
            try{
                $model = Mage::getModel('customcmsmenu/cmsmenu');//Получаем модель
                $model->setData($data)->setId($this->getRequest()->getParam('id')); // при сохранении у модели добавление айди происходит 
               
                //после инициализации данных, т.к. если вначале добавить айди, то при вызове setData айди будет перезатёрт. 
                //В итоге при сохранении вместо редактирования текущей записи, будет создана новая.
                if(!$model->getCreated()){ //проверяем ,если нет даты создания , то добавляем дату создания
                    $model->setCreated(now());
                }else{
                    $model->setUpdated(now()); //Если есть дата создания , то обновляем дату обновления
                }
                
                $model->save();//Сохраняем в БД
                
                Mage::getSingleton('adminhtml/session')->addSuccess($this->__('Menu was saved succesfully'));//Выводим сообщение что все ок
                Mage::getSingleton('adminhtml/session')->setFormData(false); //?????????????????????
                if($this->getRequest()->getParam('back'))
                {
                    $this->_redirect('*/*/edit/id/' . $model->getId());
                    return;
                }
                $this->_redirect('*/*/'); //. $this->getRequest()->getParam('back')  . '/id/' .$this->getRequest()->getParam('id')  );//Редиректим
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setFormData($data);
               
                $this->_redirect('*/*/edit' , array(
                    'id'    => $this->getRequest()->getParam('id'),
                ));
            }
            return;
        }
        Mage::getSinglton('adminhtml/session')->addError($this->__('Unable to find item to save'));//В случае отсутствия POST выводим ошибку
        
        $this->_redirect('*/*/');   
    }
    
    public function deleteAction()
    {
        if($id = $this->getRequest()->getParam('id')){ 
            try{
                Mage::getModel('customcmsmenu/cmsmenu')->setId($id)->delete(); //Удаляем Объект с id
                Mage::getSingleton('customcmsmenu/session')->addSuccess($this->__('Menu was deleted successfully')); //Говорим ,что все прошло успешно
            } catch (Exception $ex) {
                Mage::getSingleton('customcmsmenu/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $id)); //???????????????????????
            }
        }
    }
    
    public function massDeleteAction()
    {
        $menu = $this->getRequest()->getParam('menu', null); //Получаем GET menu ,(установлен в Block/Adminhtml/Cmsmenu/Grid.php метод _prepareMassaction)
        
        if(is_array($menu) && sizeof($menu) > 0){
            try {
                foreach ($menu as $id){
                    Mage::getModel('customcmsmenu/cmsmenu')->setId($id)->delete(); //Удаляем объект из БД по id
                }
                $this->_getSession()->addSuccess($this->__('Total of %d menu have been deleted', sizeof($menu)));// Сообщение об успешном удалении
            } catch (Exception $e) {
                $this->_getSesion()->addError($e->getMessage());//Сообщение об ошибке
            }
        } else {
            $this->_getSession()->addError($this->__('Please select menu')); //Ошибка в случае, если меню не выбраны
        }
        $this->_redirect('*/*');
    }
    
}