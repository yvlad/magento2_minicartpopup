<?php

class Custom_CmsMenu_Block_Adminhtml_Cmsmenu_Edit_Tabs_General extends Mage_Adminhtml_Block_Widget_Form
{
    
    protected function _prepareForm() 
    {
        $helper = Mage::helper('customcmsmenu');
        $model  = Mage::registry('current_cmsmenu');
      
        $form     = new Varien_Data_Form();
        $fieldset = $form->addFieldset('general_form', array(
            'legend' => $helper->__('General Information') 
        ));
        
        $fieldset->addField('menu_name', 'text', array(
            'label'    => $helper->__('Menu Name'),
            'required' => true,
            'name'     => 'menu_name',
        ));
        
        $fieldset->addField('created', 'hidden', array(
            'name'     => 'created',
        ));

        $fieldset->addField('status', 'select', array(
            'label'   => $helper->__('Status'),
            'default' => 0,
            'name'    => 'status',
            'type'    => 'options',
            'options' => array(
                0 => 'Disable',
                1 => 'Enable'
            )
        ));
        
        $form->setValues($model->getData());
        $this->setForm($form);

        return parent::_prepareForm();
    }
}