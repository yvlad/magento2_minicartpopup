<?php

class Custom_CmsMenu_Adminhtml_CmslinksController extends Mage_Adminhtml_Controller_Action
{
    
    
    public function newAction()
    {
        $this->_forward('edit');
    }
    
    public function editAction()
    {
        $id = (int) $this->getRequest()->getParam('id');
        Mage::register('current_cmslinks', Mage::getModel('customcmsmenu/cmslinks')->load($id));
        $model = Mage::getModel('customcmsmenu/cmslinks');
       
        $this->loadLayout()->_setActiveMenu('customcmsmenu');
        $this->_addContent($this->getLayout()->createBlock('customcmsmenu/adminhtml_cmslinks_edit')); 
        
        $this->renderLayout();
    }  
}